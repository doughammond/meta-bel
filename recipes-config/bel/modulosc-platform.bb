DESCRIPTION = "Platform configuration files for ModulOSC project."
HOMEPAGE = "http://None/"
#SECTION = "config"
PRIORITY = "optional"
LICENSE = "BSD"
PR = "r0"

SRC_URI = "file:///home/doug/dev/ModulOSC_Platform/platform-conf.tgz"

SRC_URI[md5sum] = "087af332235d8fa6fa8bcec4fb309f78"
SRC_URI[sha256sum] = "d71d81495a56a8a959d8d62b399743af08d9f2f994da19da6ad19cc9960d30f2"

LIC_FILES_CHKSUM = "file://${WORKDIR}/fsroot/LICENSE;md5=d41d8cd98f00b204e9800998ecf8427e"

S = "${WORKDIR}/fsroot"

# Ensure wpa-supplicant is installed first !
RDEPENDS_${PN} = "wpa-supplicant"

do_install() {
	# find and create all dirs
#	find ${S} -type d | sed -e 's:^${S}:${D}/:' | xargs install -d

	# find and install all files
#	find ${S} -type f -exec install -m 0644 {} ${D}/ \;

	# would be better to use install rather than cp :/
	cp -fr ${S}/* ${D}/
}

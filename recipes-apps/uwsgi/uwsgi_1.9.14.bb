DESCRIPTION = "uWSGI is a WSGI web server for Python web applications"
HOMEPAGE = "http://projects.unbit.it/uwsgi/wiki"
SECTION = "net"
PRIORITY = "optional"
LICENSE = "GPLv2"
SRCNAME = "uwsgi"
PR = "r1"

SRCREV = "1.9.14"

SRC_URI = "http://projects.unbit.it/downloads/uwsgi-1.9.14.tar.gz \
	file://arm-timer-syscall.patch \
	file://bb_package.ini \
"

#S = "${WORKDIR}/git"

DEPENDS = "libxml2 python python-core"

RDEPENDS_${PN} = "\
  python \
  python-core \
"

CFLAGS += "-DOBSOLETE_LINUX_KERNEL"

LDFLAGS = "-L${STAGING_LIBDIR} -Wl,-rpath-link,/lib -Wl,-rpath-link,/usr/lib -Wl,-rpath,/lib -Wl,-rpath,/usr/lib -Wl,-O1"

do_compile_prepend() {
    export BUILD_SYS=${BUILD_SYS}
    export HOST_SYS=${HOST_SYS}
    export STAGING_INCDIR=${STAGING_INCDIR}
    export STAGING_LIBDIR=${STAGING_LIBDIR}
}

do_compile() {
    export X86_SYSROOT=/home/doug/mnt/linux-data/yocto/yoctoProject/raspberryPiBuild/tmp/sysroots/x86_64-linux
    export X86_PYTHON=${X86_SYSROOT}/usr/bin/python-native/python2.7
    export UWSGI_BUILD_PROFILE=default

    # builds the core
    UWSGI_INCLUDES=${STAGING_INCDIR} ${X86_PYTHON} uwsgiconfig.py --build ${UWSGI_BUILD_PROFILE}

    # builds a plugin
    #UWSGI_INCLUDES=${STAGING_INCDIR} ${X86_PYTHON} uwsgiconfig.py --plugin plugins/python ${UWSGI_BUILD_PROFILE} python27
}

do_install() {
    echo ${S}

    # core binary
    install -m 0755 -d ${D}${sbindir}
    install -m 0755 ${S}/uwsgi ${D}${sbindir}

    # plugins
#    install -m 0755 -d ${D}${libdir}/uwsgi
#    install -m 0644 ${S}/python27_plugin.so ${D}${libdir}/uwsgi

    # config
#    install -m 0755 -d ${D}${sysconfdir}
#    install -m 0755 -d ${D}${sysconfdir}/uwsgi
#    install -m 0644 ${WORKDIR}/editor.ini ${D}${sysconfdir}/uwsgi
#    install -m 0644 ${WORKDIR}/public.ini ${D}${sysconfdir}/uwsgi

    # python module
    install -m 0755 -d ${D}${libdir}/python2.7/site-packages
    install -m 0755 ${S}/uwsgidecorators.py ${D}${libdir}/python2.7/site-packages
}

SRC_URI[md5sum] = "ec9cf333534604f17ef4e24051d9d65d"
SRC_URI[sha256sum] = "601bcd08c4b1073fed146dc7af24b21e0b0d353d2ebf5864df9613320fc759cd"

LIC_FILES_CHKSUM = "file://LICENSE;md5=751419260aa954499f7abaabaa882bbe"


include recipes-images/bel/bel-devel.bb

IMAGE_INSTALL += " \	
	python-smbus \
	python-pygobject \
	python-flask \
	python-markupsafe \
	python-jinja2 \
	python-werkzeug \
	nginx \
	uwsgi \
	modulosc-platform \
"

export IMAGE_BASENAME = "modulosc"

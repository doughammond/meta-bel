include recipes-core/images/rpi-basic-image.bb

RDEPENDS += " \
	python \
	python-core \
	boost \
	qt4-embedded \
"

IMAGE_INSTALL += " \
	systemd \
	networkmanager \
	qt4-embedded \
	rsync \
	usbutils \
	boost \
	mosh \
	mosh-server \
	linux-firmware \
	wireless-tools \
	wpa-supplicant \
	python-core \
	python-modules \
	python-setuptools \
	python-dbus \
	modulosc-platform \
"

IMAGE_FEATURES += "systemd ssh-server-openssh"

IMAGE_DEV_MANAGER = "udev"
IMAGE_INIT_MANAGER = "systemd"
IMAGE_INITSCRIPTS = " "

# these don't work
DISTRO_FEATURES += "systemd"
# IMAGE_FEATURES -= "splash"
# EXTRA_IMAGE_FEATURES += "ssh-server-openssh"

CONMANPKGS = "networkmanager network-manager-applet"

export IMAGE_BASENAME = "bel-devel"

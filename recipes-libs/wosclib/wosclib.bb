DESCRIPTION = "WOscLib is a complete (server and client), easy to use OSC (OpenSound-Control) library, featuring Object Oriented (C++) design (modularity), platform- independence, type-safety (especially in OSC-methods), exception-handling and good documentation (doxygen)"
SUMMARY = "WOscLib is a complete (server and client), easy to use OSC (OpenSound-Control) library, featuring Object Oriented (C++) design (modularity), platform- independence, type-safety (especially in OSC-methods), exception-handling and good documentation (doxygen)"

HOMEPAGE = "http://wosclib.sourceforge.net/"

LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=c71b653a0f608a58cdc5693ae57126bc"

SECTION = "libs"

PR = "r0"

SRC_URI = "git://git.code.sf.net/p/wosclib/code"
S = "${WORKDIR}/git/"

SRCREV = "HEAD"


inherit autotools


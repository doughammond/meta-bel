DESCRIPTION = "WiringPi library for RaspberryPi GPIO"
SUMMARY = "WiringPi library for RaspberryPi GPIO"

HOMEPAGE = "https://projects.drogon.net/raspberry-pi/wiringpi/"

LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://COPYING.LESSER;md5=e6a600fd5e1d9cbde2d983680233ad02"

SECTION = "libs"

PR = "r0"

SRC_URI = "git://github.com/WiringPi/WiringPi.git"
S = "${WORKDIR}/git/wiringPi"

SRCREV = "dda3305ce14a750e7529ff20b0acf9e53bd8eb7a"

# Fix fault in piNes.c
CFLAGS_prepend = "-I ${S} "



inherit lib_package
